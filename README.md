# REAM

This is the current project for translation of COHSHAK into R.
The R program relies on the CAs database

[Wiki page](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/home)

[REAM Package Help](https://chinook-technical-committee.gitlab.io/programs/ream/)


This a reproduction of the ERA program (CAS-CoShak).
The code has been transcribed from original VB code and the initial translation of the vb version of the program
Comments from the original version of the model were preserved.  Code was also written to maximize similarity between this version and the original Visual Basic version, functions are equivalent to the SUBs in the original code.

written in long form and with the intent to closely match the available vb code as an exercise

## Installation

To install `ream` you will need to have the R package `remotes` installed. 

In R:
```{r} 
install.packages("remotes") 
remotes::install_git('https://gitlab.com/MichaelFolkes/ream', ref = 'devfolkes')

```

<!---
commented out:

If you are using Ubuntu/Linux, for a successful install of devtools, you may need to run this line in BASH:
`sudo apt-get install libcurl4-openssl-dev libssl-dev`

In R:
```{r} 
install.packages("devtools") 
```

`ream` is currently a private repository so CTC members must include their gitlab ID and password in the installation command. **This is not a very secure method.** There is a better method, which relies on personal acess tokens and it's documented [here](https://gitlab.com/profile/personal_access_tokens).
However, if you wish to use the less secure approach, run the following code in R, and replace 'id' and 'password' with your own gitlab credentials (in double quotes). 

```{r} 
install.packages("git2r")
devtools::install_git("https://gitlab.com/chinook-technical-committee/programs/ream", credentials=git2r::cred_user_pass("id", "password"))
```

-->


Not all functions have been fully tested. 

Once the package is installed, if using Rstudio, you will (eventually) find some basic information in the user guides area of the package documentation.


If you run into problems installing Rtools on a newer version of R (4.0.0), try installing [Rtools40](https://cran.r-project.org/bin/windows/Rtools/index.html).

## Code navigation

### REAM user options, data and calculated quantities  

All stored in two R lists:

- command_data: stores user inputs  (radio buttons in coshak) and additional choices listed in the CM1 files.
- stockdata - stores the data and the stock calculations. There is one stock_data for each CM1 file - after models are run, these are stored in allcm1stock_data. 

### Main function

REAM's main function is [PrimarySub](https://gitlab.com/chinook-technical-committee/programs/ream/-/blob/master/R/ERA_PrimarySub.R#L83) all other functions are called from inside PrimarySub. If you want to run REAM as a script, there are two functions that will write and open a script for the ERA and another for the validation of output:

In R:
```{r} 
require(ream)
writeScriptPrimary()
writeScriptValidation()
```

### Initialization and object dimensions

Most objects are initialized in the function [init_stock_data](https://gitlab.com/chinook-technical-committee/programs/ream/-/blob/master/R/func_initializeobj.R#L79). If you are unsure about the dimensionality of an object, check it out there. 


### Developer scripts

Scripts used by Catarina Wor and Michael Folkes while developing code are in the `devs` folder. In order to run the scripts, you need to create an empty `devs/out` folder locally.
 

