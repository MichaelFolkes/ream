#' Check to make sure number of fisheries is the same in all subsequent C-files
#' 
#' @param command_data list of user input options
#' @param stock_data list containing stock specific information
#' @param flag Need to be provided
#' @param BY Need to be provided
#' @param Num Need to be provided
#' @param Code Need to be provided
#' @param NumFsh Need to be provided
#'
#' @export
#' @return A list containing all initial M items plus additional output from MainSub
#' 
#'
CheckNumFish <- function(command_data, stock_data, flag, BY, Num, Code, NumFsh){


  # called by sub readCfiles()
  # save number of fisheries that was read from the first C-file
  # check to make sure number of fisheries is the same in all subsequent C-files

  if(flag){
    NumFsh <- Num
  }else if(Num != NumFsh){
    if(!is.na(stock_data$allBY[BY]) & stock_data$allBY[BY] <= stock_data$LastBY){
      warning(paste0("check",command_data$outpath,"error_msgs.log"))
      sink(paste0(command_data$outpath,"error_msgs.log"), append=TRUE)
      cat( paste0(Num, " fisheries in ", stock_data$TagCodeFile[[BY]][Code],"\n"))
      cat( paste0(" does not match " , NumFsh,"\n"))
      cat( paste0(" fisheries in " , stock_data$TagCodeFile[[BY]][1],"\n"))
      cat( paste0(".  Hint:  check to make sure the first brood year in the .cm1 file matches the same in the .cds file.\n"))
      cat(paste0("number of fisheries don't agree among C-files \n"))
      sink()
     
      # do not stop program after error message
      # because some stocks do not have C-files for the first brood year
      # Call PrintError(ErrMessage, ErrCaption)
    }else{
      diff <- NumFsh - Num
      warning(paste0("check",command_data$outpath,"error_msgs.log"))
      sink(paste0(command_data$outpath,"error_msgs.log"), append=TRUE)
      cat( paste0("WARNING:  different number of fisheries in .PSL  and .CMB files \n"))
      cat( paste0( Num, " fisheries in .PSL file does not agree with " , NumFsh , " in .CMB file.\n"))
      if( diff == 2){
          cat( paste0("You can ignore this warning if the 2 fishery difference is due to ESC STRAY.\n"))
      }
      sink()
    }
  }
  return(NumFsh)
    
}

#' Check to make sure the fishery names are the same in all C-files. If this is the first CWT code for the first brood year then assign gear code .
#' 
#' @param command_data a list containg user input options
#' @param stock_data a list containing stock specific information
#' @param NameFish  Name of Cfilefisheries being checked
#' @param i Position (index) of the Cfilefishery being checked
#' @param Ms a string
#' @param BY Brood year counter 
#' @param Code flag (0 or 1) indicating whether or not the fisheries name needs to be checked. 
#'
#' @return A list containing all initial M items plus additional output from MainSub
#' 
#' @export
#' 
CheckFishNames <- function(command_data, stock_data, NameFish, i, Ms, BY, Code){

  # called by sub readCfiles() and sub getPSLdata()
  # If this is the first CWT code for the first brood year then assign gear code
  # Check to make sure the fishery names are the same in all C-files
  # fishName(i) is used to store the detailed-fishery names temporary.
  # fishName(i) will be overwritten with the combined-fishery names in sub CombineFish

  # local variable
  escName <- rep("", command_data$NumEsc)
  fishName <- stock_data$fishName[i]
  EscFlag <- stock_data$EscFlag[i]

  escName[1] <- "COST REC"
  escName[2] <- "PERS USE"
  escName[3] <- "SUB"
  escName[4] <- "STRAYS"
  escName[5] <- "CA ESC STRAY" # for use in standardized database
  escName[6] <- "US ESC STRAY" # for use in standardized database

  if(!is.na(stock_data$allBY[BY]) & stock_data$allBY[BY] == stock_data$FirstBY & Code == 1){
    gear <- ChooseFishType(NameFish)
   
    if(gear == 0){
      # ErrMessage = "Wrong Gear% Ending in Fishery Name \Tag Code= " + TagCodeFile$(by%, Code%) + " \Fishery Name= " + NameFish$
      sink(paste0(command_data$outpath,"error_msgs.log"), append=TRUE)
      cat( paste0(" the last letter in ",  NameFish , " is supposed to be T, N, S, or L.  Please check ",stock_data$TagCodeFile[[BY]][Code] ,"\n"))
      cat( paste0("problem with fishery name \n"))
      sink()
    }
    stock_data$fishName[i] <- NameFish
    stock_data$EscFlag[i] <- 0
      
    for(j in seq_len(command_data$NumEsc)){
      if( stock_data$fishName[i] == escName[j]){
        if(j %in% 1:4){
          stock_data$EscFlag[i] <- 1
        } else if(j == 5) {
          stock_data$EscFlag[i] <- 2 #Canada
        } else if(j == 6) {
          stock_data$EscFlag[i] <- 3 #US
        }
      }
    }
  }

  if(stock_data$fishName[i] != NameFish){
    # COMPARE FISHERY NAMES//REJECT IF UNSURE OF CORRESPONDENCE
    #     ErrMessage = "FISHERY HEADER MISMATCH AT: " + FishName(i)
    #     If by% <= LstBY Then
    #         ErrMessage = ErrMessage + " IN TAG CODE " + TagCodeFile$(by%, Code%)
    #     Else
    #        ErrMessage = ErrMessage + " vs " + NameFish$ + Ms$
    #     End If
    ErrMessage <- paste0(NameFish," in ")
    if(stock_data$allBY[BY] <= stock_data$LastBY & !is.na(stock_data$allBY[BY])){
      ErrMessage <- paste0(ErrMessage,stock_data$TagCodeFile[[BY]][Code], "does not match ", NameFish ,".  Ignore if first brood year in CDS file does not have C-files.")
    }else{
      ErrMessage <- paste0(ErrMessage,Ms, " does not match ", fishName, " in the .cmb file.  Hint #1:  If the mismatched name ", NameFish, " is valid, then ignore this warning message. \n")
      ErrMessage <- paste0(ErrMessage, "  Hint #2:  If the mismatched name is not right, then look for a comma that is out of place or duplicate row in the line just before ", fishName, " in the .psl file\n")
      ErrMessage <- paste0(ErrMessage, "  Hint #3:  Compare the order of the fisheries in the .psl file with the .cmb file.\n")
    }
    sink(paste0(command_data$outpath,"error_msgs.log"), append=TRUE)
    cat("mismatched fishery name\n")
    cat(ErrMessage)
    sink()
    stop(paste("NameFish is ", NameFish," and fishName",i , "is", stock_data$fishName[i]))
  }

  return(stock_data)


  #------------------------------------------------------------------------------------------------
  #Original vb code - lines 3079- 3356
  #------------------------------------------------------------------------------------------------
  #    'called by sub readCfiles() and sub getPSLdata()
  #        'If this is the first CWT code for the first brood year then assign gear code
  #        'Check to make sure the fishery names are the same in all C-files
  #        'fishName(i) is used to store the detailed-fishery names temporary.
  #        'fishName(i) will be overwritten with the combined-fishery names in sub CombineFish
  #
  #        'local variable
  #        Dim escName(NumEsc) As String
  #        Dim j, gear As Integer
  #    escName(0) = "COST REC"
  #        escName(1) = "PERS USE"
  #        escName(2) = "SUB"
  #        escName(3) = "STRAYS"
  #        escName(4) = "CA ESC STRAY" 'for use in standardized database
  #        escName(5) = "US ESC STRAY" 'for use in standardized database
  #        If by = FirstBY And Code = 1 Then
  #            gear = ChooseFishType(NameFish)
  #            If gear = 0 Then
  #                '        ErrMessage = "Wrong Gear% Ending in Fishery Name \Tag Code= " + TagCodeFile$(by%, Code%) + " \Fishery Name= " + NameFish$
  #                ErrMessage = " the last letter in " & NameFish & " is supposed to be T, N, S, or L.  Please check " & TagCodeFile(by, Code)
  #                ErrCaption = "problem with fishery name"
  #                '        Call PrintError(ErrMessage, ErrCaption)
  #                MsgBox(ErrMessage, 0, ErrCaption)
  #            End If
  #            fishName(i) = NameFish
  #            EscFlag(i) = 0
  #            For j = 0 To NumEsc
  #                If fishName(i) = escName(j) Then
  #                    Select Case j
  #                        Case 0, 1, 2, 3
  #                            EscFlag(i) = 1
  #                        Case 4
  #                            EscFlag(i) = 2 'Canada
  #                        Case 5
  #                            EscFlag(i) = 3 'US
  #                    End Select
  #                    'WriteLine(traceEscStray_ID, "checkFishNames", by + 1900, i, fishName(i), EscFlag(i))
  #                End If
  #            Next j
  #        End If
  #        If fishName(i) <> NameFish Then
  #            'COMPARE FISHERY NAMES//REJECT IF UNSURE OF CORRESPONDENCE
  #            '    ErrMessage = "FISHERY HEADER MISMATCH AT: " + FishName(i)
  #            '    If by% <= LstBY Then
  #            '        ErrMessage = ErrMessage + " IN TAG CODE " + TagCodeFile$(by%, Code%)
  #            '    Else
  #            '       ErrMessage = ErrMessage + " vs " + NameFish$ + Ms$
  #            '    End If
  #            ErrMessage = NameFish & " in "
  #            If by <= LastBY Then
  #                ErrMessage = ErrMessage & TagCodeFile(by, Code) & " does not match " & fishName(i) & ".  Ignore if first brood year in CDS file does not have C-files."
  #            Else
  #                ErrMessage = ErrMessage & Ms & " does not match " & fishName(i) & " in the .cmb file.  Hint #1:  If the mismatched name " & NameFish & " is valid, then ignore this warning message."
  #                ErrMessage = ErrMessage & "  Hint #2:  If the mismatched name is not right, then look for a comma that is out of place or duplicate row in the line just before " & fishName(i) & " in the .psl file"
  #                ErrMessage = ErrMessage & "  Hint #3:  Compare the order of the fisheries in the .psl file with the .cmb file."
  #            End If
  #            ErrCaption = "mismatched fishery name"
  #            '    Call PrintError(ErrMessage, ErrCaption)
  #            MsgBox(ErrMessage, 0, ErrCaption)
  #        End If

}








#' ChooseFishType
#'
#' Called by sub CheckFishNames and calcExploitRates
#' 
#' @param FishName Fish Name
#'
#' @return return the following values \enumerate{
#'  \item First if the last letter in the fishery name is "T" gear = 1
#' \item Second if the last letter in the fishery name is "N" gear = 2
#' \item Third if the last letter in the fishery name is "S" gear = 3
#' \item Forth if Cost Recovery, Strays, Pers Use, or Subsistence gear = 4
#' \item Fifth otherwise, gear = 0
#' \item Sixth note:gear is not saved as a fishery Property
#' }
#' 
#' @export
#' 
#' 
ChooseFishType <- function(FishName){

    # called by sub CheckFishNames and calcExploitRates
    # return the following values
    # if the last letter in the fishery name is "T" gear = 1
    # if the last letter in the fishery name is "N" gear = 2
    # if the last letter in the fishery name is "S" gear = 3
    # if Cost Recovery, Strays, Pers Use, or Subsistence gear = 4
    # otherwise, gear = 0
    # note:gear is not saved as a fishery Property

    #####find out if fishery ends with TNS
    CseFishType <- NA

    temp <- gregexpr(pattern=substr(FishName,nchar(as.character(FishName)),nchar(as.character(FishName))),"TNS")[[1]][1]

    if(temp < 0)temp <- 0
    CseFishType <-  temp

    if(grepl("TRAP", FishName)){
        CseFishType <- 2
    }
    if(grepl("COST", FishName)|grepl("PERS", FishName)|grepl("SUB", FishName)){
        CseFishType <- 4
    }
    if(grepl("TERM STRAY", FishName)){
        # added for TCA and TUS TERM STRAY
              CseFishType <- 5
    }
    if(grepl("ESC STRAY", FishName)){
        # added for TCA and TUS TERM STRAY
              CseFishType <- 6
    }
    return(CseFishType)

  #------------------------------------------------------------------------------------------------
  #    #Original vb code - lines 2082- 2104
  #------------------------------------------------------------------------------------------------
  # called by sub CheckFishNames and calcExploitRates
  # return the following values
  # if the last letter in the fishery name is "T" gear = 1
  # if the last letter in the fishery name is "N" gear = 2
  # if the last letter in the fishery name is "S" gear = 3
  # if Cost Recovery, Strays, Pers Use, or Subsistence gear = 4
  # otherwise, gear = 0
  # note:gear is not saved as a fishery Property
  # temp = InStr("TNS", Microsoft.VisualBasic.Right(FishName, 1))
  # ChooseFishType = temp
  # code for AKS gear
  # If (Microsoft.VisualBasic.Right(FishName, 4) = "TRAP") Then
  #    ChooseFishType = 2
  #End If
  #If (Microsoft.VisualBasic.Left(FishName, 4) = "COST") Or (Microsoft.VisualBasic.Left(FishName, 4) = "PERS") Or (Microsoft.VisualBasic.Left(FishName, 3) = "SUB") Then
  #    ChooseFishType = 4
  #End If
  #If Microsoft.VisualBasic.Right(FishName, 10) = "TERM STRAY" Then ' added for TCA and TUS TERM STRAY
  #    ChooseFishType = 5
  #End If
  #If Microsoft.VisualBasic.Right(FishName, 9) = "ESC STRAY" Then ' added for TCA and TUS TERM STRAY
  #    ChooseFishType = 6
  #End If


}





#' TestBY 
#'
#' keep track of largest release. brood year is complete if brood(BY%).NumAge = 5 or 6
#' 
#' @param command_data A list containing user input preferences
#' @param stock_data A ist containing stock specific information
#' @param  BY Brood year counter
#' 
#' @return A list containing all initial stock_data items with modifications
#' 
#' @export
#' 
TestBY <- function(command_data,stock_data,  BY){

  # keep track of largest release
  # brood year is complete if brood(BY%).NumAge = 5 or 6

  if (stock_data$MaxRelease < stock_data$CWTRelease[BY]){
    stock_data$MaxRelease <- stock_data$CWTRelease[BY]
  }

  #print(paste("NumAge[BY]", stock_data$NumAge[BY]))
  #print(paste("MaxStkAge",stock_data$MaxStkAge))

  if (stock_data$MaxStkAge < stock_data$NumAge[BY]){
    # sub readCM1file defined MaxStkAge% = 5
    # sub readCfiles may read a C-file where brood(BY%).NumAge = 6
    #  in that case, let MaxStkAge% = 6
    stock_data$MaxStkAge <- stock_data$NumAge[BY]
  }

  if(stock_data$NumAge[BY] == stock_data$MaxStkAge){
    stock_data$numComplBY <- stock_data$numComplBY + 1
    stock_data$CompleteBYFlag[BY] <- TRUE
  }


  return(stock_data)

  #===========================================================
  #Original vb code - lines 5751-5764
  #===========================================================
  #    keep track of largest release
  #        'brood year is complete if brood(BY%).NumAge = 5 or 6
  #        If MaxRelease < CWTRelease(by) Then MaxRelease = CWTRelease(by)
  #        If MaxStkAge < NumAge(by) Then
  #             sub readCM1file defined MaxStkAge% = 5
  #             sub readCfiles may read a C-file where brood(BY%).NumAge = 6
  #              in that case, let MaxStkAge% = 6
  #            MaxStkAge = NumAge(by)
  #        End If
  #        If NumAge(by) = MaxStkAge Then
  #            numComplBY = numComplBY + 1
  #            CompleteBYFlag(by) = True
  #        End If
}







#' CheckCompleteBY
#'
#' make sure first brood years has ages 2-5 (or 6)
#' and incomplete brood year (e.g. jacks only) does not
#' preceed a complete brood year (e.g. ages 2-5 or 6)
#' local variable
#' 
#' @param command_data A list containing user input preferences
#' @param stock_data A ist containing stock specific information
#' 
#' @export
CheckCompleteBY <- function(command_data, stock_data){

  LastCompleteBYFlg <- TRUE
  for(BY in seq_along(stock_data$allBY)){
    
    if(stock_data$MissingBYFlg[BY]){     
      #BY <- FindBY(stock_data,BY)
      next
    }
    if(!stock_data$CompleteBYFlag[BY] & stock_data$allBY[BY] == stock_data$FirstBY){
      # This can only happen if a complete Brood Year is preceeded by an incomplete BY
      sink(paste0(command_data$outpath, stock_data$Stock , "incompletebrood.log"), append=T)
      cat("Hint: check the C-files from brood year ", (stock_data$allBY[BY] - 1) , " and see if the number of ages is less than the C-files in brood year " , stock_data$allBY[BY] , ".\n")
      cat("For example, did the number of ages change from " , stock_data$NumAge[BY- 1]  , " to " , stock_data$NumAge[BY]  , " and then back to " ,stock_data$NumAge[BY- 1] , " again in the C-files?\n")
      cat("This program is going to stop.\n") 
      sink()
      stop("complete Brood preceeded by incomplete BY\n")
    }
    if(stock_data$CompleteBYFlag[BY] & !LastCompleteBYFlg){
      stop("complete Brood preceeded by incomplete BY")
   
    #            This can only happen if a complete Brood Year is preceeded by an incomplete BY
    #            ErrMessage = "Hint: check the C-files from brood year " & (BY - 1) & " and see if the number of ages is less than the C-files in brood year " & BY & "."
    #            ErrMessage = ErrMessage & Chr(13) & Chr(13) & "For example, did the number of ages change from " & NumAge(BY) - 1 & " to " & NumAge(BY) & " and then back to " & NumAge(BY) - 1 & " again in the C-files?"
    #            ErrMessage = ErrMessage & Chr(13) & Chr(13) & "This program is going to stop."
    #            ErrCaption = "complete Brood preceeded by incomplete BY"
    #                    Call PrintError(ErrMessage, ErrCaption)
    #            MsgBox(ErrMessage, 0, ErrCaption)   
    }
    if (!stock_data$CompleteBYFlag[BY] & LastCompleteBYFlg){
      LastCompleteBYFlg <- FALSE
    }
  }


    #===================================================================
    #Original VB code - Lines 1933- 1958
    #==================================================================
    #     make sure first brood years has ages 2-5 (or 6)
    #     and incomplete brood year (e.g. jacks only) does not
    #     preceed a complete brood year (e.g. ages 2-5 or 6)
    #     local variable
    #    Dim LastCompleteBYFlg As Boolean
    #    LastCompleteBYFlg = True
    #    For BY = FirstBY To LastBY
    #        If MissingBYFlg(BY) = True Then BY = FindBY(BY)
    #        If CompleteBYFlag(BY) = False And BY = FirstBY Then
    #            ErrMessage = "ALL AGE CLASSES MUST BE PRESENT IN THE FIRST BROOD YEAR FOR THIS ANALYSIS"
    #            ErrCaption = "missing age data in brood year " & BY
    #            '        Call PrintError(ErrMessage, ErrCaption)
    #            MsgBox(ErrMessage, 0, ErrCaption)
    #        End If
    #        If CompleteBYFlag(BY) = True And LastCompleteBYFlg = False Then
    #            'This can only happen if a complete Brood Year is preceeded by an incomplete BY
    #            ErrMessage = "Hint: check the C-files from brood year " & (BY - 1) & " and see if the number of ages is less than the C-files in brood year " & BY & "."
    #            ErrMessage = ErrMessage & Chr(13) & Chr(13) & "For example, did the number of ages change from " & NumAge(BY) - 1 & " to " & NumAge(BY) & " and then back to " & NumAge(BY) - 1 & " again in the C-files?"
    #            ErrMessage = ErrMessage & Chr(13) & Chr(13) & "This program is going to stop."
    #            ErrCaption = "complete Brood preceeded by incomplete BY"
    #            '        Call PrintError(ErrMessage, ErrCaption)
    #            MsgBox(ErrMessage, 0, ErrCaption)
    #            End
    #        End If
    #        If CompleteBYFlag(BY) = False And LastCompleteBYFlg = True Then LastCompleteBYFlg = False
    #    Next BY

}





#' FindBY
#'
#' give a brood year where Brood(BY%).MissingByFlg = true
#' return the next brood year where Brood(BY%).MissingByFlg = false
#' 
#' @param stock_data A ist containing stock specific information
#' @param BY Brood year counter
#'
#' 
#' @export
#' 
FindBY <- function(stock_data, BY){

  while (stock_data$MissingBYFlg[BY]){
    BY <- BY + 1 
  }

  return(BY)

    

  #======================================
  #Original VB code - Line 2310-2316
  #======================================
  # give a brood year where Brood(BY%).MissingByFlg = true
  # return the next brood year where Brood(BY%).MissingByFlg = false
  #Do
  #    by = by + 1
  #Loop Until MissingBYFlg(by) = False
  #FindBY = by


}




#' SetBY
#'
#' Derive either brood and calendar year from age as appropriate
#' 
#' @param command_data Not Specified
#' @param stock_data A list containing stock specific information
#' @param YR year counter
#' @param Age Age counter
#'
#' @export
#' 
SetBY <- function(command_data, stock_data, YR, Age){

  #derive either brood and calendar year from age as appropriate
  if(stock_data$ShakCalcFlg == command_data$CalendarYR){
    BY <- YR - Age
    yr_ <- YR
  }else{
    BY <- YR - stock_data$StartAge
    yr_ <- BY + Age
  }
  return(list(BY=BY,yr_=yr_))

  #======================================
  #Original VB code - Line 5651-5660
  #======================================
  #derive either brood and calendar year from age as appropriate
  #    If ShakCalcFlg = CalendarYR Then 'CalendarYr = 1
  #        BY = YR - Age
  #        yr_ = YR
  #    Else 'ShakCalcFlg% = 0
  #        BY = YR - StartAge
  #        yr_ = BY + Age
  #    End If
  
}




#' FindTerminal 
#'
#' Check if a fishery is terminal or not, based on gear and age of fish
#' 
#' @param stock_data A ist containing stock specific information
#' 
#' @return A list containing all initial stock_data items with modifications
#' 
#' @export
#' 
FindTerminal <- function(stock_data) {

    #fishery(i).terminal( j) = true (1) for terminal fisheries
    #and net fisheries (where age is assumed to be of mature fish i.e. Terminal)
    #TermNetSwitchAge is, by convention, set to age 4, the age at which all
    #ocean net catch is assumed to be of mature fish (i.e. Terminal).
    #MaxStkAge% is typically age 5 and sometimes age 6 except for
    #incomplete broods where MaxStkAge% could be 2, 3 or 4.  Therefore
    #check to be sure that TermNetSwitchAge <= MaxStkAge%
    #i.e. age 4 and older
    #local variables

    stock_data$terminal <- matrix(FALSE, nrow=stock_data$NumStdFisheries,ncol=stock_data$MaxStkAge)

    for(i in seq_len(stock_data$NumStdFisheries)){
        tmp0 <- as.character(stock_data$fishName[i])
        tmp <- substr(tmp0, 1, 1)
        tmp2 <- substr(tmp0, nchar(tmp0), nchar(tmp0))
        if(tmp == "T" ){?substr
            stock_data$terminal[i, ] <- TRUE
        }else if(tmp == "X"){
            stock_data$terminal[i, ] <- TRUE
        }else if(tmp2 == "N" & stock_data$TermNetSwitchAge <= stock_data$MaxStkAge){
            stock_data$terminal[i, stock_data$TermNetSwitchAge <=(1:stock_data$MaxStkAge)] <- TRUE           
        }

    }

    return(stock_data)



    #======================================
    #Original VB code - Line 2325-2351
    #======================================
    #fishery(i).terminal( j) = true (1) for terminal fisheries
    #and net fisheries (where age is assumed to be of mature fish i.e. Terminal)
    #TermNetSwitchAge is, by convention, set to age 4, the age at which all
    #ocean net catch is assumed to be of mature fish (i.e. Terminal).
    #MaxStkAge% is typically age 5 and sometimes age 6 except for
    #incomplete broods where MaxStkAge% could be 2, 3 or 4.  Therefore
    #check to be sure that TermNetSwitchAge <= MaxStkAge%
    #i.e. age 4 and older
    #local variables
    #Dim i, j As Integer
    #For i = 1 To NumStdFisheries
    #    If Microsoft.VisualBasic.Left(fishName(i), 1) = "T" Then
    #        For j = StartAge To MaxStkAge
    #            terminal(i, j) = True
    #        Next j
    #    ElseIf Microsoft.VisualBasic.Left(fishName(i), 1) = "X" Then
    #        For j = StartAge To MaxStkAge
    #            terminal(i, j) = True
    #        Next j
    #    ElseIf Microsoft.VisualBasic.Right(fishName(i), 1) = "N" And TermNetSwitchAge <= MaxStkAge Then
    #        For j = TermNetSwitchAge To MaxStkAge
    #            terminal(i, j) = True
    #        Next j
    #    End If
    #Next i



}







#' CheckNumAges
#'
#' called by sub readCfiles()
#' save number of ages from the first c-file of each brood
#' check to make sure all subsequent C-files within the same brood have the same number of ages
#' because all c-files within a brood is supposed to have the same max age
#' suggested change, compare all c-files with the first c-file of the first brood year
#' in order to make sure all c-files within a stock have the same max age
#' however, need a routine to deal with incomplete broods where NumA
#' 
#' @param command_data A list containing user input preferences
#' @param stock_data A ist containing stock specific information
#' @param BY brood year counter
#' @param Num max age for the cohort
#' @param Code CWT code counter for brood year
#' 
#' @return A list containing all initial stock_data items with modifications
#' 
#' @export
#' 
CheckNumAges <- function(command_data, stock_data, BY, Num, Code){
  
  NumAge <- stock_data$NumAge[BY]
 
  if(Code == 1){
    NumAge <- Num
  }else if(Num > stock_data$NumAge[BY]){
    sink(paste0(command_data$outpath, "nageserror.log"), append=T)
    cat(Num, "ages in " , stock_data$TagCodeFile[[BY]][Code]," does not match ",stock_data$NumAge[BY], " ages in ", stock_data$TagCodeFile[[BY]][1],"\n")
    cat("Max age within a brood and within a stock should be the same. \n")
    cat("Hint: Fall chinook should be max age 5 and spring chinook max age 6 but there are some exceptions. \n")
    cat("Warning: This program will check if max age is the same within a brood year but not within a stock.\n")
    cat("Hint: Run the program checkMaxAge.exe to make sure all c-files have the same max age and that brood year + max age = last CWR recovery year within a stock.\n")
    sink()
    stop("max age for all C-files within a brood year must be the same.\n")
  }
   
  return(NumAge)

  #======================================
  #Original VB code - Line 2027-2051
  #======================================
  #called by sub readCfiles()
  #save number of ages from the first c-file of each brood
  #check to make sure all subsequent C-files within the same brood have the same number of ages
  #because all c-files within a brood is supposed to have the same max age
  #suggested change, compare all c-files with the first c-file of the first brood year
  #in order to make sure all c-files within a stock have the same max age
  #however, need a routine to deal with incomplete broods where NumAge is less than max age
  #
  #If Code = 1 Then
  #    NumAge(by) = Num
  #ElseIf Num > NumAge(by) Then
  #    '    ErrMessage = "UNEQUAL Age% CLASSES IN TAG CODE " + brood(by%).TagCodeFile(Code%)
  #    ErrMessage = Num & "ages in " & TagCodeFile(by, Code) & " does not match " & NumAge(by) & " ages in " & TagCodeFile(by, 1)
  #    ErrMessage = ErrMessage & Chr(13) & Chr(13) & "Max age within a brood and within a stock should be the same."
  #    ErrMessage = ErrMessage & Chr(13) & Chr(13) & "Hint: Fall chinook should be max age 5 and spring chinook max age 6 but there are some exceptions."
  #    ErrMessage = ErrMessage & Chr(13) & Chr(13) & "Warning: This program will check if max age is the same within a brood year but not within a stock."
  #    ErrMessage = ErrMessage & Chr(13) & Chr(13) & "Hint: Run the program checkMaxAge.exe to make sure all c-files have the same max age and that brood year + max age = last CWR recovery year within a stock."
  #    ErrCaption = "max age for all C-files within a brood year must be the same"
  #    Call PrintError(ErrMessage, ErrCaption)
  #End If
}
