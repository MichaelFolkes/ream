# 2 dimensional object
array(1:6, dim=c(3,2))
dim2 <- data.frame(1:3, 4:6)
dim2

# 3 dimensional object 
array(1:18, dim=c(3,2,2))
dim3 <- list(dim2, dim2)
dim3

# 4 dimensional object
array(1:18, dim=c(3,2,2,3))
dim4 <- list(dim3, dim3, dim3)
dim4

# 5 dimensional object
array(1:18, dim=c(3,2,2,3,4))
dim5 <- list(dim4, dim4, dim4, dim4)
dim5


#____________
# using arrays

rows <- 2
cols <- 3
levels <- 4

dimnames <- list(paste0("row", 1:rows), paste0("col", 1:cols), paste0("level", 1:levels))

x <- array(1:24,dim = c(2,3,4), dimnames = dimnames)
x[,,c("level1", "level2")]
